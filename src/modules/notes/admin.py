from django.contrib import admin

from modules.notes import models


@admin.register(models.Label)
class LabelAdmin(admin.ModelAdmin):
    search_fields = ['name']


@admin.register(models.Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = ['last_modified_at', 'author', 'title']
    search_fields = ['title', 'content']
    readonly_fields = ['slug']
    ordering = ['-last_modified_at']
    filter_horizontal = ['labels']

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'labels':
            kwargs['queryset'] = models.Label.objects.filter(author=request.user)
        return super().formfield_for_manytomany(db_field, request, **kwargs)
