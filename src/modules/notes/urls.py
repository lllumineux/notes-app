from rest_framework import routers

from modules.notes import viewsets


router = routers.DefaultRouter()

router.register('labels', viewsets.LabelViewSet)
router.register('notes', viewsets.NoteViewSet)
