from rest_framework import serializers

from modules.notes import models


class LabelSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['author'] = self.context['request'].user
        return super().create(validated_data)

    class Meta:
        model = models.Label
        fields = '__all__'
        read_only_fields = ['author']


class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Note
        fields = '__all__'
        read_only_fields = ['slug', 'author']
        lookup_field = 'slug'
