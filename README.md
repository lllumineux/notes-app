# Notes App

## How to start?
Simply run `docker-compose up`.

## What are the endpoints?
Visit http://localhost:8000/swagger to discover.
