from rest_framework import viewsets, permissions

from modules.notes import serializers
from modules.notes import models
from modules.notes import permissions as custom_permissions


class LabelViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.LabelSerializer
    queryset = models.Label.objects.filter()
    permission_classes = [permissions.IsAuthenticated & (permissions.IsAdminUser | custom_permissions.IsAuthor)]

    def get_queryset(self):
        if self.request.user.is_superuser:
            return super().get_queryset()
        return self.queryset.filter(author=self.request.user)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class NoteViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.NoteSerializer
    queryset = models.Note.objects.filter()
    permission_classes = [permissions.IsAuthenticated & (permissions.IsAdminUser | custom_permissions.IsAuthor)]
    lookup_field = 'slug'

    def get_queryset(self):
        if self.request.user.is_superuser:
            return super().get_queryset()
        return self.queryset.filter(author=self.request.user)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
