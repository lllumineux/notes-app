from django.apps import AppConfig


class NotesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modules.notes'

    def ready(self):
        from modules.notes import signals  # noqa: F401
