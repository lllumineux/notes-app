from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


class Label(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    author = models.ForeignKey(User, related_name='labels', verbose_name=_('Author'), on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.author}: {self.name}'

    class Meta:
        verbose_name = _('Label')
        verbose_name_plural = _('Labels')
        constraints = [
            models.UniqueConstraint(fields=['author', 'name'], name='unique_user_label_name')
        ]


class Note(models.Model):
    title = models.CharField(_('Title'), max_length=200)
    slug = models.SlugField(_('Slug'), max_length=200, help_text=_('Generated automatically'))
    content = RichTextUploadingField(_('Content'), blank=True, null=True)
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    last_modified_at = models.DateTimeField(_('Last modified at'), auto_now=True)
    labels = models.ManyToManyField(Label, related_name='notes', verbose_name=_('Labels'), blank=True)
    author = models.ForeignKey(User, related_name='notes', verbose_name=_('Author'), on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.author}: {self.slug}'

    class Meta:
        verbose_name = _('Note')
        verbose_name_plural = _('Notes')
        constraints = [
            models.UniqueConstraint(fields=['author', 'title'], name='unique_user_note_title'),
            models.UniqueConstraint(fields=['author', 'slug'], name='unique_user_note_slug')
        ]
