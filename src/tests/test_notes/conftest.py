import pytest
from rest_framework.test import APITestCase, APIClient

from tests.test_notes import factories


@pytest.mark.django_db
class InitUser(APITestCase):
    def init_users(self):
        self.user = factories.UserFactory(username='user')
        self.admin = factories.UserFactory(username='admin', is_superuser=True)

    def authorize_users(self) -> None:
        self.user_authorized = APIClient()
        response_for_user = self.user_authorized.post(
            '/api/auth/token/',
            {'username': 'user', 'password': 'qwerty12345'}
        )
        self.user_access_token = response_for_user.json()['access']
        self.user_authorized.credentials(HTTP_AUTHORIZATION='Bearer ' + self.user_access_token)

        self.admin_authorized = APIClient()
        response_for_admin = self.admin_authorized.post(
            '/api/auth/token/',
            {'username': 'admin', 'password': 'qwerty12345'}
        )
        self.admin_access_token = response_for_admin.json()['access']
        self.admin_authorized.credentials(HTTP_AUTHORIZATION='Bearer ' + self.admin_access_token)

    def setUp(self):
        self.init_users()
        self.authorize_users()


class InitLabel(InitUser):
    def init_labels(self):
        self.user_label = factories.LabelFactory(name='User label', author=self.user)
        self.admin_label = factories.LabelFactory(name='Admin label', author=self.admin)

    def setUp(self):
        super().setUp()
        self.init_labels()


class InitNote(InitLabel):
    def init_notes(self):
        self.user_note = factories.NoteFactory(title='User note', author=self.user, labels=[self.user_label])
        self.admin_note = factories.NoteFactory(title='Admin note', author=self.admin, labels=[self.admin_label])

    def setUp(self):
        super().setUp()
        self.init_notes()
