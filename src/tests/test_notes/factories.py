import factory
from django.contrib.auth import models
from django.contrib.auth.hashers import make_password

from modules.notes import models as note_models


class UserFactory(factory.django.DjangoModelFactory):
    username = 'username'
    email = f'{username}@email.com'
    password = make_password('qwerty12345')
    is_superuser = False

    class Meta:
        model = models.User


class LabelFactory(factory.django.DjangoModelFactory):
    name = 'This is label name'
    author = factory.SubFactory(UserFactory)

    class Meta:
        model = note_models.Label


class NoteFactory(factory.django.DjangoModelFactory):
    title = 'This is note title'
    content = '<p>This is note content</p>'
    author = factory.SubFactory(UserFactory)

    @factory.post_generation
    def labels(self, create, extracted, **kwargs):
        _ = kwargs
        if not create:
            return
        if extracted:
            for label in extracted:
                self.labels.add(label)

    class Meta:
        model = note_models.Note
