from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.text import slugify

from modules.notes import models


@receiver(pre_save, sender=models.Note)
def my_callback(sender, instance, *args, **kwargs):
    _ = sender, args, kwargs
    instance.slug = slugify(instance.title)
