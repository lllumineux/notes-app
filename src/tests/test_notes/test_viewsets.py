from tests.test_notes.conftest import InitNote, InitLabel


class TestLabelViewSet(InitLabel):
    def test_admin_create(self):
        url = '/api/labels/'
        data = {'name': 'Admin test label'}
        response = self.admin_authorized.post(url, data)
        response_data = response.json()
        assert response.status_code == 201
        assert response_data['author'] == self.admin.pk
        assert response_data['name'] == data['name']

    def test_admin_retrieve(self):
        url = f'/api/labels/{self.admin_label.pk}/'
        response = self.admin_authorized.get(url)
        response_data = response.json()
        assert response.status_code == 200
        assert response_data['name'] == 'Admin label'

    def test_user_own_retrieve(self):
        url = f'/api/labels/{self.user_label.pk}/'
        response = self.user_authorized.get(url)
        response_data = response.json()
        assert response.status_code == 200
        assert response_data['name'] == 'User label'

    def test_user_stranger_retrieve(self):
        url = f'/api/labels/{self.admin_label.pk}/'
        response = self.user_authorized.get(url)
        assert response.status_code == 404

    def test_user_list(self):
        url = f'/api/labels/'
        response = self.user_authorized.get(url)
        response_data = response.json()
        assert response.status_code == 200
        assert len(list(filter(lambda x: x['id'] == self.user_label.id, response_data))) == 1
        assert len(list(filter(lambda x: x['author'] != self.user.id, response_data))) == 0


class TestNoteViewSet(InitNote):
    def test_admin_create(self):
        url = '/api/notes/'
        data = {
            'title': 'Admin test note title',
            'content': '<p>Admin test note content</>',
            'labels': [self.admin_label.id]
        }
        response = self.admin_authorized.post(url, data)
        response_data = response.json()
        assert response.status_code == 201
        assert response_data['author'] == self.admin.pk
        assert response_data['title'] == data['title']
        assert response_data['labels'] == data['labels']

    def test_admin_retrieve(self):
        url = f'/api/notes/{self.admin_note.slug}/'
        response = self.admin_authorized.get(url)
        response_data = response.json()
        assert response.status_code == 200
        assert response_data['title'] == 'Admin note'

    def test_user_own_retrieve(self):
        url = f'/api/notes/{self.user_note.slug}/'
        response = self.user_authorized.get(url)
        response_data = response.json()
        assert response.status_code == 200
        assert response_data['title'] == 'User note'

    def test_user_stranger_retrieve(self):
        url = f'/api/notes/{self.admin_note.slug}/'
        response = self.user_authorized.get(url)
        assert response.status_code == 404

    def test_user_list(self):
        url = f'/api/notes/'
        response = self.user_authorized.get(url)
        response_data = response.json()
        assert response.status_code == 200
        assert len(list(filter(lambda x: x['slug'] == self.user_note.slug, response_data))) == 1
        assert len(list(filter(lambda x: x['author'] != self.user.id, response_data))) == 0
